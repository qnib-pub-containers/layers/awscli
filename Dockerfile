FROM alpine:3.15 AS py
RUN apk add --no-cache py3-pip \
 && pip install --no-cache-dir awscli

FROM alpine:3.15
COPY --from=py /usr/lib /usr/lib
COPY --from=py /usr/bin /usr/bin